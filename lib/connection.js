var mongoose = require('mongoose');
var dbUrl = 'mongodb://localhost/hr';

mongoose.connect(dbUrl);

// Close the mongoose connection on Control + C

process.on('SIGINT', function () {
    mongoose.connection.close(function () {
        console.log('\nMongoose default connection disconnected');
        process.exit(0);
    });
});

require('../models/employee');
require('../models/team');
