/**
 * Created by matthewdelbuono on 2015-11-29.
 */

var fs = require('fs');

exports.send404 = function (response) {
    console.error("Response Not Found");

    response.writeHead(404, {'Content-Type': 'text/plain'});
    response.end("Not Found")
};

exports.sendJson = function (data, response) {
    response.writeHead(200, {'Content-Type': 'application/json'});
    response.end(JSON.stringify(data));
};

exports.send500 = function (data, response) {
    console.error(data);

    response.writeHead(500, {'Content-Type': 'plain/text'});

    response.end(data);
};

exports.staticFile = function (staticPath) {
    return function (data, response) {
        var readStream;

        // fix so routes to /home and /home.html both work
        data = data.replace(/^(\/home)(.html)?$/i, '$1.html');
        data = '.' + staticPath + data;

        fs.stat(data, function (error, stats) {
            if (error || stats.isDirectory()) {
                return exports.send404(response);
            }
            readStream = fs.createReadStream(data);
            return readStream.pipe(response);
        });
    }
};