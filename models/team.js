/**
 * Created by matthewdelbuono on 2015-12-27.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var postFind = require('mongoose-post-find');
var async = require('async');
var TeamSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    members: {
        type: [Schema.Types.Mixed]
    }
});

function _attachMembers(Employee, result, callback) {
    console.info(result);
    Employee.find({
        team: result._id
    }, function (error, employees) {
        if (error) return callback(error);

        result.members = employees;
        callback(null, result);
    });
}

// listen for find and findOne
TeamSchema.plugin(postFind, {
    find: function (result, callback) {
        //console.info('FIND');
        var Employee = mongoose.model('Employee');

        async.each(result, function (item, callback) {
            _attachMembers(Employee, item, callback);

        }, function (error) {
            if (error) return callback(error);
            callback(null, result);
        });
    },
    findOne: function (result, callback) {
        //console.info('FIND');
        var Employee = mongoose.model('Employee');
        _attachMembers(Employee, result, callback);
    }
});

module.exports = mongoose.model('Team', TeamSchema);